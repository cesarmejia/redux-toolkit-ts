import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface CurrentDisplay {
  displayType: 'issues' | 'comments'
  issueId: number | null
}

//# Action.Payload Type for setCurrentDisplayType action creator
interface CurrentDisplayPayload {
  displayType: 'issues' | 'comments'
  issueId?: number
}

//# Action.Payload Type for displayRepo Action Creator
interface CurrentRepo {
  org: string
  repo: string
}

//# Intersection Type for stateSlice object
type CurrentDisplayState = {
  page: number
} & CurrentDisplay &
  CurrentRepo

//# state parameter in case reducers inherits this static type
let initialState: CurrentDisplayState = {
  org: 'rails',
  repo: 'rails',
  page: 1,
  displayType: 'issues',
  issueId: null
}

const issuesDisplaySlice = createSlice({
  name: 'issues',
  initialState,
  reducers: {
    displayRepo(state, action: PayloadAction<CurrentRepo>) {
      const { org, repo } = action.payload
      state.org = org
      state.repo = repo
    },
    setCurrentPage(state, action: PayloadAction<number>) {
      state.page = action.payload
    },
    setCurrentDisplayType(state, action: PayloadAction<CurrentDisplayPayload>) {
      const { displayType, issueId = null } = action.payload
      state.displayType = displayType
      state.issueId = issueId
    }
  }
})

export const {
  displayRepo,
  setCurrentPage,
  setCurrentDisplayType
} = issuesDisplaySlice.actions

export default issuesDisplaySlice.reducer
