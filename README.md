# 1. Redux Toolkit Tutorials

<!-- TOC -->

- [Redux Toolkit Tutorials](#redux-toolkit-tutorials)
  - [Timeline](#timeline)
  - [QuickStart](#quickstart)
  - [Basic Tutorial](#basic-tutorial)
  - [Intermediate Tutorial](#intermediate-tutorial)
  - [Advance Tutorial](#advance-tutorial)

<!-- /TOC -->

## 1.1. Timeline

- [x] ~~_QuickStart_~~ [2020-01-02]
- [x] ~~_Basic Tutorial_~~ [2020-01-02]
- [x] ~~_Intermediate Tutorial_~~ [2020-01-03]
- [x] ~~_Advance Tutorial_~~ [2020-01-03]

## 1.2. QuickStart

- **Redux Toolkit** is intended to be the standard way to write Redux logic.

- `RTK` addresses three common concerns:

  1. "Configuring a Redux store is too complicated"
  2. "I have to add a lot of packages to get Redux to do anything useful"
  3. "Redux requires too much boilerplate code"

- `RTK` does not address:

  1. "how to create reusable encapsulated Redux modules"
  2. data fetching
  3. folder or file structures
  4. managing entity relationships in the store

- `RTK` includes:

  - `configureStore()`, a function with simplied configuration options

    - it can combine your slice reducers
    - add Redux middleware
    - includes `redux-thunk` by default
    - enables `Redux DevTools Extension` by default

  - `createReducer()`, a utility that lets your supply a _lookup table_ of _action types_ to case reducer functions. Automatically uses `immer` to let you write immutable "mutative code"

  - `createAction()`, a utility that returns an _action creator_ function for the given _action type_ string. `toString()` method returns _type constant_

  - `createSlice()`, a function that accepts a _set of reducer functions_, a _slice name_, an _initial state value_, and automatically generates a _slice reducer_ w/ corresponding _action creators_ and _action types_

  - `createSelector()`, a utility from `Reselect` library

## 1.3. Basic Tutorial

- `configureStore()`:

```javascript
// Before:
const store = createStore(counter)
// After:
const store = configureStore({
  reducer: counter,
})
```

- `createAction()`:

```javascript
// Original approach: write the action type and action creator by hand
const INCREMENT = 'INCREMENT'
function incrementOriginal() {
  return { type: INCREMENT }
}
console.log(incrementOriginal())
// {type: "INCREMENT"}
// Or, use `createAction` to generate the action creator:
const incrementNew = createAction('INCREMENT')
console.log(incrementNew())
// {type: "INCREMENT"}
```

- If you need _action type_

```javascript
const increment = createAction('INCREMENT')
console.log(increment.toString())
// "INCREMENT"
console.log(increment.type)
// "INCREMENT"
```

- `createReducer()`:

```javascript
const counter = createReducer(0, {
  [increment]: state => state + 1,
  [decrement]: state => state - 1,
})
```

- **NOTE:** The above works because the _computed properties syntax_ will call `toString()`

- _Why do we even need to generate the action creators separately, or write out those action type strings? **The really important part here is the reducer functions.**_

- `createSlice({})`:

  1. `name`, slice name
  2. `initialState`, the initial state
  3. `reducers`, object that serves as a lookup table of action types to reducer functions

- `const stateSlice = createSlice({})`
  - returns `actions` object and `reducer` function

```javascript
const counterSlice = createSlice({
  name: 'counter',
  initialState: 0,
  reducers: {
    increment: state => state + 1,
    decrement: state => state - 1,
  },
})

const { actions, counterReducer } = counterSlice
const { increment, decrement } = actions

const store = configureStore({
  reducer: counterReducer,
})
```

## 1.4. Intermediate Tutorial

- `npm i @reduxjs/toolkit`

- _A normal Redux application has a JS object at the top of its state tree._

- A **slice** is one key/value pair of the Redux state object. A **slice reducer** is the _reducer function_ responsible for updating a specific field in the Redux state object.

- `createSlice` takes an _options object_ as its argument with 3 fields:

  1. `name`: a _string_ that is used as the prefix for generated action types
  2. `initialState`: the _initial state value_ for the reducer
  3. `reducers`: an _object_, keys are _action types_ (strings); values are _reducer functions_ to run when said action type is dispatched (i.e. `case reducers`)

- `createSlice` wrap reducer with `Immer`'s `produce` method.

- `createSlice` **returns** an _object_:

```javascript
{
  name: "todos",
  reducer: (state, action) => newState,
  actions: {
    addTodo: (payload) => ({type: "todos/addTodo", payload}),
    toggleTodo: (payload) => ({type: "todos/toggleTodo", payload})
  },
  caseReducers: {
    addTodo: (state, action) => newState,
    toggleTodo: (state, action) => newState,
  }
}
```

- `ducks pattern`, _it suggests that you should put all your action creators and reducers in one file, do **named exports** of the **action creators**, and **default export** of the **reducer function**_

- _It's up to the reducer to establish what it thinks `payload` should be for each action type, and whatever code dispatches the action needs to pass in values that match that expectation._

- `createAction` takes a second argument, a _prepare callback_, that shapes the action.payload

  - can receive arguments from the invoking component and \*_must_ return an object with a `payload` field. **It may** also include a `meta` field.

- `createSlice`, to customize action creator, the `case reducer` is now an object with a `reducer` and `prepare` methods

```javascript
let nextTodoId = 0
const todosSlice = createSlice({
  name: 'todos',
  initialState: [],
  reducers: {
    addTodo: {
      reducer(state, action) {
        const { id, text } = action.payload
        state.push({ id, text, completed: false })
      },
      prepare(text) {
        return { payload: { text, id: nextTodoId++ } }
      }
    }
  }
}
```

- `getVisibleTodos` is a _selector fucntion_. It encapsulates the process of reading values from the Redux store and extracting part or all of those values for use.

```javascript
const getVisibleTodos = (todos, filter) => {
  switch (filter) {
    case VisibilityFilters.SHOW_ALL:
      return todos
    case VisibilityFilters.SHOW_COMPLETED:
      return todos.filter(t => t.completed)
    case VisibilityFilters.SHOW_ACTIVE:
      return todos.filter(t => !t.completed)
    default:
      throw new Error('Unknown filter: ' + filter)
  }
}

const mapStateToProps = state => ({
  todos: getVisibleTodos(state.todos, state.visibilityFilter),
})
```

- `createSelector`, from `Reselect` library, lets you define _memoized_ selector functions. _These memoized selectors only recalculate values if the inputs have actually changed._

## 1.5. Advance Tutorial

- In the Advanced RTK Tutorial, we will learn:

  1. How to convert a "plain React" app to use Redux
  2. How async logic like data fetching fits into RTK
  3. How to use RTK with TypeScript

- Starting code is a Github Issues viewer app

- _We're going to want to know what the TypeScript type is for the root state object, because we need to declare what the type of the state variable is whenever our code needs to access the Redux store state_:

  - `mapState` functions
  - `useSelector` selectors
  - `getState` in thunks

  - In `rootReducer.ts`:

  ```javascript
  export type RootState = ReturnType<typeof rootReducer>
  ```

- **Hot Module Replacement**:

```javascript
if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./rootReducer', () => {
    const newRootReducer = require('./rootReducer').default
    store.replaceReducer(newRootReducer)
  })
}
```

- ??? I thought CRA already handled Hot Module Reloading

- _The `require('./rootReducer').default` is the **default export**_

- `createSlice` tries to infer types from two sources:

  - The `state` type is based on the type of the `initialState`
  - Each reducer needs to declare the type of action it expects to handle

- The main type you will use when declaring action types in reducers is **PayloadAction<PayloadType>**

```javascript
setCurrentPage(state, action: PayloadAction<number>) {
      state.page = action.payload
    },
```

- Need to make _three groups of changes_ to the `App` component:
  1. Remove `useState` declarations
  2. Corresponding `state` values need to be read from Redux store
  3. Redux actions need to be dispatched in place of `setState` updater functions

```javascript
function exampleThunkFunction(dispatch, getState) {
  // do something useful with dispatching or the store state here
}
// normally an error, but okay if the thunk middleware is added
store.dispatch(exampleThunkFunction)
```

- A `thunked` action creator:

```javascript
function exampleThunk() {
  return function exampleThunkFunction(dispatch, getState) {
    // do something useful with dispatching or the store state here
  }
}
// normally an error, but okay if the thunk middleware is added
store.dispatch(exampleThunk())
```

- **Why Thunks?**

  - Thunks allow us to write reusable logic that interacts with a Redux store, _but without needing to reference a specific store instance_
  - Thunks enable us _to move complex logic outside of our components_

- `Redux Toolkit` does not currently provide any special functions or syntax for writing thunk functions. _In particular, they cannot be defined as part of a `createSlice()` call._

- `thunk functions` are collocated with their respective `sliceReducer`.

- _The `AppThunk` type declares that the "action" that we're using is specifically a thunk function._
  1. Return value: the thunk doesn't return anything
  2. State type for `getState`: returns our RootState type
  3. "Extra argument": the thunk middleware can be customized to pass in an extra value
  4. Action types accepted by `dispatch`: any action who `type` is a string

```javascript
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>
```

- `AJAX Thunk`

```javascript
export const fetchIssuesCount = (org: string, repo: string): AppThunk => async dispatch => {
  try {
    const repoDetails = await getRepoDetails(org, repo)
    dispatch(getRepoDetailsSuccess(repoDetails))
  } catch (err) {
    dispatch(getRepoDetailsFailed(err.toString()))
  }
}
```

- **Important to Note:**
  1. The thunk is defined separately from the slice.
  2. We declare the thunk action creator as as an arrow function, and use the AppThunk type we just created.
  3. We use the `async/await` syntax for the `thunk` function itself.
  4. Inside the thunk, we `dispatch` the plain action creators that were generated by the `createSlice` call

```markdown
However, this leads to a performance problem. Every time this selector runs, it returns a new object: {commentsLoading, commentsError, comments}. Unlike connect, useSelector relies on reference equality by default. So, returning a new object will cause this component to rerender every time an action is dispatched, even if the comments are the same!

There's a few ways to fix this:

We could write those as separate useSelector calls
We could use a memoized selector, such as createSelector from Reselect
We can use the React-Redux shallowEqual function to compare the results, so that the re-render only happens if the object's contents have changed.
```
